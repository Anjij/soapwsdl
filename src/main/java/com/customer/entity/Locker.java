package com.customer.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LOCKER_TAB")
public class Locker {
	@Id

	private int lockerid;

	private String lockertype;

	private double rent;

	public int getLockerid() {
		return lockerid;
	}

	public void setLockerid(int lockerid) {
		this.lockerid = lockerid;
	}

	public String getLockertype() {
		return lockertype;
	}

	public void setLockertype(String lockertype) {
		this.lockertype = lockertype;
	}

	public double getRent() {
		return rent;
	}

	public void setRent(double rent) {
		this.rent = rent;
	}

	@Override
	public String toString() {
		return "Locker [lockerid=" + lockerid + ", lockertype=" + lockertype + ", rent=" + rent + "]";
	}

}
