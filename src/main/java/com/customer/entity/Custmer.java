package com.customer.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CUSTMER_TAB")
public class Custmer {
	@Id
	
	private int custmerid;
	
	@Column(length=20)
	private String custmername;
	
	
	@OneToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="Lockerid_fk",referencedColumnName="lockerid",unique=true)
	private Locker locker;


	public int getCustmerid() {
		return custmerid;
	}


	public void setCustmerid(int custmerid) {
		this.custmerid = custmerid;
	}


	public String getCustmername() {
		return custmername;
	}


	public void setCustmername(String custmername) {
		this.custmername = custmername;
	}


	public Locker getLocker() {
		return locker;
	}


	public void setLocker(Locker locker) {
		this.locker = locker;
	}


	@Override
	public String toString() {
		return "Custmer [custmerid=" + custmerid + ", custmername=" + custmername + ", locker=" + locker + "]";
	}
	

}
