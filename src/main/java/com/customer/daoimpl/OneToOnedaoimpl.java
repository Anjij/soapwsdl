package com.customer.daoimpl;

import javax.jws.WebMethod;
import javax.jws.WebService;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.customer.dao.OneToOne;
import com.customer.entity.Custmer;
import com.customer.util.HibernateUtil;

@WebService(endpointInterface = "com.app.dao.OneToOne")
public class OneToOnedaoimpl implements OneToOne {

	public void saveCustmer(Custmer custmer) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction txt = session.beginTransaction();
		session.save(custmer);
		txt.commit();
		session.close();

	}

	public void searchbyid(Integer custmerid) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Custmer custmer = (Custmer) session.get(Custmer.class, custmerid);
		System.out.println(custmer);
		session.close();

	}

	public void deletebyid(Integer custmerid)

	{
		Session session = HibernateUtil.getSessionFactory().openSession();
		Custmer custmer = (Custmer) session.get(Custmer.class, custmerid);
		System.out.println(custmer);
		session.close();

	}

}
